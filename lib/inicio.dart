import 'package:flutter/material.dart';
import 'package:flutter_formularios/barra_navegacion/barra_pantalla.dart';
import 'package:flutter_formularios/formularios/formularios_pantalla.dart';

class MyInicio extends StatelessWidget {
  const MyInicio({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("FORMULARIOS"),
        backgroundColor: Colors.amber,
        elevation: 12,
      ),
      body: const MyFormulario(),
      bottomNavigationBar: const MyBarra(),
    );
  }
}
