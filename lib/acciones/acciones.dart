import 'package:cloud_firestore/cloud_firestore.dart';

class Acciones {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  Future registrar({String? correo, String? telefono, String? mensaje}) async {
    final CollectionReference usuarios = db.collection('usuarios');

    final Map<String, dynamic> informacion = {
      "correo": correo,
      "telefono": telefono,
      "mensaje": mensaje
    };

    return await usuarios.add(informacion);
  }

  Future listar() async {
    final FirebaseFirestore db = FirebaseFirestore.instance;
    final CollectionReference usuarios = db.collection('usuarios');

    QuerySnapshot<Object?> datos = await usuarios.get();

    return datos;
  }

  Future actualizar(
      {String? id, String? correo, String? telefono, String? mensaje}) async {
    final FirebaseFirestore db = FirebaseFirestore.instance;
    final CollectionReference usuarios = db.collection('usuarios');

    final Map<String, dynamic> informacion = {
      "correo": correo,
      "telefono": telefono,
      "mensaje": mensaje
    };

    return await usuarios.doc(id).set(informacion);
  }

  Future borrarId(
      {String? id, String? correo, String? telefono, String? mensaje}) async {
    final FirebaseFirestore db = FirebaseFirestore.instance;
    final CollectionReference usuarios = db.collection('usuarios');
    return await usuarios.doc(id).delete();
  }

  Future borrarTodo(
      {String? id, String? correo, String? telefono, String? mensaje}) async {
    final FirebaseFirestore db = FirebaseFirestore.instance;
    final CollectionReference usuarios = db.collection('usuarios');
    QuerySnapshot<Object?> datos = await usuarios.get();
    for (var doc in datos.docs) {
      await doc.reference.delete();
    }
  }
}
