import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:flutter_formularios/acciones/acciones.dart';
import 'package:flutter_formularios/barra_navegacion/barra_pantalla.dart';

class Empresa extends StatefulWidget {
  const Empresa({super.key});

  @override
  State<Empresa> createState() => _EmpresaState();
}

class _EmpresaState extends State<Empresa> {
  Acciones ac = Acciones();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Empresa"),
      ),
      bottomNavigationBar: const MyBarra(),
      body: Column(
        children: [
          Text("MOSTRAR DATOS"),
          StreamBuilder<QuerySnapshot>(
              stream:
                  FirebaseFirestore.instance.collection("usuarios").snapshots(),
              builder: ((context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  final datos = snapshot.data!.docs;
                  return ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemBuilder: ((context, index) {
                      return Row(
                        children: [
                          Text(datos[index]["correo"]),
                          ElevatedButton(
                              onPressed: () {}, child: const Icon(Icons.edit)),
                          ElevatedButton(
                              onPressed: () {
                                ac.borrarTodo();
                              },
                              child: const Icon(Icons.delete))
                        ],
                      );
                    }),
                    itemCount: datos.length,
                  );
                } else {
                  return const Text("SIN DATOS");
                }
              }))
        ],
      ),
    );
  }
}
